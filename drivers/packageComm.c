#include <stdint.h>
#include <string.h>
#include "uart.h"
#include "packageComm.h"

#define START_BYTE 0xAA
#define PACKAGE_SIZE 32
#define PACKAGE_MASK (PACKAGE_SIZE-1)

static uint8_t package[PACKAGE_SIZE];
static uint8_t packageIndex = 0;
static uint16_t packageChecksum;

extern int autoTransmitPackages;
extern int flags;

void PC_transmitConfirmPackage(PackageValue_t value)
{
	uint16_t checksum = 0;
	UART_putchar((char) START_BYTE);
	checksum += (uint16_t) START_BYTE;
	UART_putchar(2);
	checksum += (2);
	UART_putchar((char) PT_CONFIRM);
	checksum += (uint16_t) PT_CONFIRM;
	UART_putchar((char) value);
	checksum += (uint16_t) value;
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	UART_putchar((uint8_t) (~checksum));
}

void PC_transmitMessage(char *message)
{
	PC_transmitDataPackage(PV_MESSAGE, (uint8_t*) message, strlen(message));
}

void PC_transmitDataPackage(PackageValue_t value, uint8_t * payload, uint8_t length)
{
	int i;
	uint16_t checksum = 0;
	UART_putchar((char) START_BYTE);
	checksum += (uint16_t) START_BYTE;
	UART_putchar(length + 2);
	checksum += (length + 2);
	UART_putchar((char) PT_DATA);
	checksum += (uint16_t) PT_DATA;
	UART_putchar((char) value);
	checksum += (uint16_t) value;
	for (i = 0; i < length; i++) {
		UART_putchar(payload[i]);
		checksum += payload[i];
	}
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	UART_putchar((uint8_t) (~checksum));
}

void PC_transmitPackage(PackageType_t type, uint8_t * payload, uint8_t length)
{
	int i;
	uint16_t checksum = 0;
	UART_putchar((char) START_BYTE);
	checksum += (uint16_t) START_BYTE;
	UART_putchar(length + 1);
	checksum += length + 1;
	UART_putchar((char) type);
	checksum += (uint16_t) type;
	for (i = 0; i < length; i++) {
		UART_putchar(payload[i]);
		checksum += payload[i];
	}
	while (checksum > 0x100) {
		checksum = (checksum >> 8) + (checksum & 0xFF);
	}
	UART_putchar((uint8_t) (~checksum));
}

static void interpretSet(uint8_t *package)
{
	PackageValue_t pv = package[3];
	switch (pv) {
	case PV_AUTOTRANSMIT:
		autoTransmitPackages = package[4];
		PC_transmitConfirmPackage(PV_AUTOTRANSMIT);
		break;
	case PV_SETFLAG: {
		uint16_t mask;
		memcpy((void*) &mask, (const void*) &package[4], sizeof(uint16_t));
		flags |= mask;
		PC_transmitConfirmPackage(PV_SETFLAG);
	}
		break;
	case PV_CLEARFLAG: {
		uint16_t mask;
		memcpy((void*) &mask, (const void*) &package[4], sizeof(uint16_t));
		flags &= ~mask;
		PC_transmitConfirmPackage(PV_CLEARFLAG);
	}
		break;
	case PV_FLAG: // "dangerous", use with caution
		memcpy((void*) &flags, (const void*) &package[4], sizeof(uint16_t));
		PC_transmitConfirmPackage(PV_FLAG);
		break;
	case PV_MEMORY: {
		unsigned int address = (package[4] + (package[5] << 8));
		unsigned int value = (package[6] + (package[7] << 8));
		memcpy((void*) address, (const void*) &value, sizeof(uint16_t));
		PC_transmitConfirmPackage(PV_MEMORY);
	}
		break;
	default:
		break;
	}
}

static void interpretGet(uint8_t *package)
{

	PackageValue_t pv = package[3];
	switch (pv) {
	case PV_AUTOTRANSMIT:
		PC_transmitDataPackage(PV_AUTOTRANSMIT, (void*) &autoTransmitPackages, 1);
		break;
	case PV_FLAG:
		PC_transmitDataPackage(PV_FLAG, (void*) &flags, sizeof(uint16_t));
		break;
	case PV_MEMORY: {
		unsigned int addresse = (unsigned int) package[4] + ((unsigned int) package[5] << 8);
		PC_transmitDataPackage(PV_MEMORY, (void*) (addresse), package[6]);
	}
		break;
	default:
		break;
	}
}

static void interpretPackage(uint8_t *package)
{
	PackageType_t pt = (PackageType_t) package[2];
	switch (pt) {
	case PT_GET: {
		interpretGet(package);
	}
		break;
	case PT_DATA: // ignore
		break;
	case PT_SET:
		interpretSet(package);
		break;
	case PT_CONFIRM:
		break;
	}
}

void PC_processPackage(uint8_t b)
{
	switch (packageIndex) {
	case 0:
		if (b == START_BYTE) {
			package[0] = b;
			packageIndex = 1;
			packageChecksum = b;
		}
		break;
	case 1:
		package[1] = b;
		packageChecksum += b;
		packageIndex = 2;
		break;
	default:
		package[packageIndex] = b;
		packageChecksum += b;
		packageIndex++;
		if (packageIndex >= package[1] + 3) {
			packageIndex = 0;
			while (packageChecksum > 0x100) {
				packageChecksum = (packageChecksum >> 8) + (packageChecksum & 0xFF);
			}
			packageChecksum = (~packageChecksum) & 0xFF;
			if (packageChecksum == 0) {
				interpretPackage(package);
			} else {
				PC_transmitMessage("CS ERR");
			}
		}
		break;
	}
}

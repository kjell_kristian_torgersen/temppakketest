#include <avr/io.h>
#include <avr/interrupt.h>

// Initialisering av ADC tatt fra eksempel pĺ internett. ADCen overklokkes for ĺ kjřre pĺ 16e6/(13*32) = 38461.538 Hz.
// Dette medfřrer at de 2 minst signifikante bitene blir dĺrlige, men de brukes ikke likevell. Dette gjřr vi for ĺ klare hřyere bitrate/fĺ bedre střyimmunitet pĺ lavere bitrate

void ADC10_init(void)
{
	// clear ADLAR in ADMUX (0x7C) to right-adjust the result
	// ADCL will contain lower 8 bits, ADCH upper 2 (in last two bits)
	ADMUX = (1<<ADLAR)|(0<<REFS1)|(1<<REFS0);
	// Set ADEN in ADCSRA (0x7A) to enable the ADC.
	// Note, this instruction takes 12 ADC clocks to execute
	ADCSRA |= 0B10000000;

	// Set ADATE in ADCSRA (0x7A) to enable auto-triggering.
	ADCSRA |= 0B00100000;

	// Clear ADTS2..0 in ADCSRB (0x7B) to set trigger mode to free running.
	// This means that as soon as an ADC has finished, the next will be
	// immediately started.
	ADCSRB &= 0B11111000;

	// Set the Prescaler to 128 (16000KHz/128 = 125 kHz)
	// Above 200KHz 10-bit results are not reliable.
	ADCSRA |= 0B00000111;

	// Set ADIE in ADCSRA (0x7A) to enable the ADC interrupt.
	// Without this, the internal interrupt will not trigger.
	ADCSRA |= 0B00001000;

	// Enable global interrupts
	// AVR macro included in <avr/interrupts.h>, which the Arduino IDE
	// supplies by default.
	sei();

	// Set ADSC in ADCSRA (0x7A) to start the ADC conversion
	ADCSRA |= 0B01000000;
}

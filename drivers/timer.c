/*
* Timer.c
*
* Created: 07.11.2015 15:08:20
*  Author: Kjell
*/

#include <avr/io.h>
#include <avr/interrupt.h>

volatile unsigned int TIMER_ms = 0;
volatile unsigned int TIMER_s = 0;

unsigned int TIMER_GetSeconds()
{
	unsigned int timer;
	cli();
	timer = TIMER_s;
	sei();

	return timer;
}

unsigned int TIMER_GetMillis()
{
	unsigned int timer;
	cli();
	timer = TIMER_ms;
	sei();

	return timer;
}

void TIMER_Init(void)
{
	TCCR1B |= (1 << WGM12);
	OCR1A = 15999; //et mssekund
	TIMSK1 |= (1 << OCIE1A);
	TCCR1B |= ((1 << CS10) | (0 << CS12)); // 16MHz
}


unsigned int TIMER_ms2 = 0;
ISR(TIMER1_COMPA_vect)
{
	TIMER_ms++;
	TIMER_ms2++;
	if(TIMER_ms2 >= 1000) {
		TIMER_s++;
		TIMER_ms2 = 0;
	}
}

/*
 * uartDrv.c
 *
 *  Created on: 15. aug. 2012
 *      Author: dnh
 */

/*
 * main.c
 *
 *  Created on: 13. aug. 2012
 *      Author: dnh
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "uart.h"
#include "config.h"

CircBuff_t uartTxQ, uartRxQ;
extern int flags;

void UART_putchar(char c)
{

	uartTxQ.Data[uartTxQ.Head] = c;
	uartTxQ.Head = (uartTxQ.Head + 1) & (BUFF_SIZE - 1);

	if (uartTxQ.Head == uartTxQ.Tail) {
		// Error, head has catched up with tail, set flag
		flags |= FLAG_UARTTX_OVERRUN;
	}
}

void UART_Init(unsigned char ubrr)
{
	uartTxQ.Head = 0;
	uartTxQ.Tail = 0;
	uartRxQ.Head = 0;
	uartRxQ.Tail = 0;

	UCSR0B = ((1 << RXEN0) | (1 << TXEN0)); // Turn on the transmission and reception circuitry
	UCSR0C = ((0 << UMSEL00) | (1 << UCSZ00) | (1 << UCSZ01)); // Use 8-bit character sizes
	//UCSR0A |= U2X0;
	UBRR0 = ubrr;
	//UBRR0 = (((F_CPU / (baudrate * 8UL))) - 1UL);
	UCSR0B |= (1 << RXCIE0); // Enable the USART Recieve Complete interrupt (USART_RXC)
}

void UART_prosess_TxQ(void)
{
	if (uartTxQ.Head != uartTxQ.Tail) {
		if (UCSR0A & (1 << UDRE0)) {
			UDR0 = uartTxQ.Data[uartTxQ.Tail];
			uartTxQ.Tail = (uartTxQ.Tail + 1) & (BUFF_SIZE - 1);
		}
	}
}

int UART_getchar()
{
	int ret = -1;
	if (uartRxQ.Head != uartRxQ.Tail) {
		ret = uartRxQ.Data[uartRxQ.Tail];
		uartRxQ.Tail = (uartRxQ.Tail + 1) & (BUFF_SIZE - 1);
	}

	return ret;
}

ISR(USART_RX_vect)
{
	uartRxQ.Data[uartRxQ.Head] = UDR0;
	uartRxQ.Head = (uartRxQ.Head + 1) & (BUFF_SIZE - 1);
	if (uartRxQ.Head == uartRxQ.Tail) {
		flags |= FLAG_UARTRX_OVERRUN;
	}

}


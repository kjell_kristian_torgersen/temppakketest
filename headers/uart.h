/*
 * uartDrv.h
 *
 *  Created on: 15. aug. 2012
 *      Author: dnh
 */

#ifndef UARTDRV_H_
#define UARTDRV_H_

#define BUFF_SIZE	0x40

typedef struct {
	unsigned char Data[BUFF_SIZE];
	unsigned char Head;
	unsigned char Tail;
} CircBuff_t;

void UART_putchar(char c);
void UART_Init(unsigned char ubrr);
void UART_prosess_TxQ(void);
int UART_getchar();

#endif /* UARTDRV_H_ */

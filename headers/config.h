/*
 * config.h
 *
 *  Created on: Jan 24, 2016
 *      Author: kjell
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#define FLAG_UARTRX_OVERRUN 1
#define FLAG_UARTTX_OVERRUN 2
#define FLAG_PID_ENABLE		4
#define FLAG_AUTO_TRANSMIT_PACKAGES 8

#endif /* CONFIG_H_ */

/*
 * packageComm.h
 *
 *  Created on: 4. des. 2015
 *      Author: Kjell
 */

#ifndef HEADERS_PACKAGECOMM_H_
#define HEADERS_PACKAGECOMM_H_

typedef enum PackageTag {
	PT_GET = 0, PT_SET = 1, PT_DATA = 2, PT_CONFIRM = 3
} PackageType_t;

typedef enum PackageValueTag {PV_PID, PV_PROCESS, PV_AUTOTRANSMIT, PV_MESSAGE, PV_SETFLAG, PV_CLEARFLAG, PV_FLAG, PV_SETPOINT, PV_RESETPROCESS, PV_POWER, PV_MEMORY } PackageValue_t;

void PC_transmitDataPackage(PackageValue_t value, uint8_t * payload, uint8_t length);
void PC_transmitPackage(PackageType_t type, uint8_t * payload, uint8_t length);
void PC_processPackage(uint8_t b);
void PC_transmitMessage(char *message);

#endif /* HEADERS_PACKAGECOMM_H_ */

/*
 * timer.h
 *
 * Created: 07.11.2015 15:15:20
 *  Author: Kjell
 */


#ifndef TIMER_H_
#define TIMER_H_

unsigned int TIMER_GetSeconds();
unsigned int TIMER_GetMillis();
void TIMER_Init(void);

#endif /* TIMER_H_ */
